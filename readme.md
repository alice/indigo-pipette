# indigo-pipette

Pipette is a simple functional framework for creating Python pipelines. In this context, a pipeline is series of pipeline *stages* connected via function calls. A pipeline stage is a *unit of execution* which takes some input, produces some output. The output of one stage is fed as input to the next, forming a chain that as a whole implements some complex or multi-faceted operation. The difference between pipelining and ordinary function composition, is that pipeline stages are augmented into Python generators. This allows you to seamlessly process data collections without ever resorting to loops - i.e. your pipeline can be implemented with an *declarative* rather than imperative paradigm.

Pipette is also helpful in notebook environments because it allows you define an operation piecewise, while letting you execute the process as a whole - no notebook restarts are required.


## Combinators

Pipette is implemented using several basic building blocks called function combinators. As in combinator parsing, these can be arbitrarily combined to produce complex operations. The combinators supported so far are outlined below:

### `map(function)`

Map applies a given function to every element of an iterable. For example:

```py
input = iter([1, 2, 3, 4])

square = map(lambda x: x ** 2)
```

Applying `square` to `input` will produce a new iterable, we can `next()` into:

```py
output = square(input)
```

```py
next(output) # returns 1
next(output) # returns 4 (2 squared is 4)
list(output) # returns [9, 16] (the remaining items)
```

Map (like all combinators discussed here), can also be used with decorator syntax:

```py
@map
def add_one(x):
  return x + 1
```

This is functionally equivalent to:

```py
add_one = map(lambda x: x + 1)
```


### `filter(predicate)`

Filter runs a predicate over every element of an interable and returns only those elements which satisfy the predicate. For example:

```py
@filter
def if_lowercase(string):

  if string == string.lower():
    return True
```


```py
strings = [
  'hello',
  'world',
  'abC',
  'abc',
  'ABC'
]

strings_lowercase = if_lowercase(strings)
```

```py
list(strings_lowercase) # returns ['hello', 'world', 'abc']
```


### `reduce(function)`

Reduce takes a binary function and calls it on the previous result and the next value in an iterable. For example, the reduction of `[1, 2, 3, 4]` with binary addition is `((1 + 2) + 3) + 4 = 10`.

```py
numbers = [1, 2, 3, 4]
```

```py
reduce_add = reduce(lambda x, y: x + y)
```

```
result = reduce_add(numbers)

next(result) # returns 01
```


### `serialise(function)`

This combinator works much like the spread operator in JavaScript or array flattening. Serialise takes a pipeline stage that returns collections (e.g. arrays), and returns an iterable that contains the values. For example:


```py
@serialse
def characters(string):
  return list(string)
```

```py
string = 'hello'
chars = characters(string)

next(chars) # returns 'h'
next(chars) # returns 'e'
next(chars) # returns 'l'
```


### `collect(function)`

Collect accumulates all of the values yielded by a generator, and yields a single collection. It's more or less the inverse of `serialise()`.


### `pipe(*stages)`

This function creates a pipeline given a number of pipeline stages. It's a shorthand for manually nesting function calls.


```py
pipeline = pipe(
  map(lambda x: x ** 2),
  filter(lambda x: x % 2 == 0),
)
```

```py
input = [1, 2, 3, 4, 5, 6]

output = pipeline(input)
```

```py
next(output) # returns 4
next(output) # returns 16
next(output) # returns 36
```

Since the result of `pipeline()` is just another pipeline stage, you can connect multiple pipelines and nest them. This allows your code to remain highly modular.


## Pipe

The last feature of pipette is a utility class called `Pipe`. This allows you to create pipelines in a more object-oriented style if you prefer this. For example, the previous example would be implemented as follows:

```py
pipeline = Pipe()

pipeline.then(map(lambda x: x ** 2))
pipeline.then(filter(lambda x: x % 2 == 0))
```

```py
input = [1, 2, 3, 4, 5, 6]

output = pipeline(input)
```

You can also add multiple stages in a single call to `Pipe.then()` by passing in more than one argument.


## Recap

In only 77 lines pipette offers a great deal of functionality for streamlining complex data processing code. Originally, this project's aim was to simplify preprocessing during web scraping, but its concepts are applicable to a far wider range of tasks. We hope you enjoy! Please report any bugs and issues to this repository's issue tracker.
